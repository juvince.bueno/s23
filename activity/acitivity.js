// Task #3
db.rooms.insert({
    name: "single",
    accomodates: 2,
    price: 1000.00,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
});

// Task #4
db.rooms.insertMany([
{
    name: "double",
    accomodates: 3,
    price: 2000.00,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false
},
{
    name: "queen",
    accomodates: 4,
    price: 4000.00,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false
}
]);


// Task #5
db.rooms.find({
    
    name: "double"
    
    });

// Task #6
db.rooms.updateOne(
    { name: "queen" },
	    {
	        $set : {
	            name: "queen",
                        accomodates: 3,
                        price: 2000.00,
                        description: "A room with a queen sized bed perfect for a simple getaway",
                        rooms_available: 0,
                        isAvailable: false
	        }
	    }
);

// Task #7

db.rooms.deleteMany(
		{rooms_available: 0}
	);